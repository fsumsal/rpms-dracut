From f814aed234f5bfb99029b9926a41727cbc68824c Mon Sep 17 00:00:00 2001
From: Tao Liu <ltao@redhat.com>
Date: Fri, 3 Mar 2023 18:27:25 +0800
Subject: [PATCH] fix(lvmthinpool-monitor): activate lvm thin pool before
 extend its size

The state of lvm thin pool may change into inactived state when kdump into
2nd kernel. As a result, lvextend will fail to extend its size. For example:

In 1st kernel:

  $ lvs
  LV       VG         Attr       LSize   Pool     Origin Data%  Meta%
  vol      vg00       Vwi-a-tz-- 300.00m thinpool        70.54
  thinpool vg00       twi-aotz-- 236.00m                 89.67  12.50
  ...

In 2nd kernel:

  LV       VG         Attr       LSize   Pool     Origin Data%  Meta%
  vol      vg00       Vwi-aotz-- 300.00m thinpool        70.54
  thinpool vg00       twi---tz-- 236.00m                 89.67  12.50
  ...

And the lvextend failing log:
[  8.483878] start-thinpool-monitor[590]: lvextend using policy requires the thin pool to be active.

In this patch, we will first activate lvm thin pool before calling lvextend
during kdump.

Signed-off-by: Tao Liu <ltao@redhat.com>

(Cherry-picked commit: 4a6b81378f5c542fa1a94386744b9f4a0cc3e33a)

Resolves: #2175096
---
 modules.d/80lvmthinpool-monitor/start-thinpool-monitor.sh | 6 ++++++
 1 file changed, 6 insertions(+)

diff --git a/modules.d/80lvmthinpool-monitor/start-thinpool-monitor.sh b/modules.d/80lvmthinpool-monitor/start-thinpool-monitor.sh
index 75d8eada..10f4a4bb 100755
--- a/modules.d/80lvmthinpool-monitor/start-thinpool-monitor.sh
+++ b/modules.d/80lvmthinpool-monitor/start-thinpool-monitor.sh
@@ -31,6 +31,12 @@ if [ -n "$THIN_POOLS" ]; then
         CONFIG="activation {monitoring=0 thin_pool_autoextend_threshold=70 thin_pool_autoextend_percent=20}"
     fi
 
+    # Activate the thinpool in case the thinpool is in inactive state.
+    # Otherwise lvextend will fail.
+    for THIN_POOL in $THIN_POOLS; do
+        lvm lvchange -ay "$THIN_POOL" --config "$CONFIG"
+    done
+
     while true; do
         for THIN_POOL in $THIN_POOLS; do
             lvm lvextend --use-policies --config "$CONFIG" "$THIN_POOL"
